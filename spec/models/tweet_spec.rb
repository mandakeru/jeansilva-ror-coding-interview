require 'rails_helper'

RSpec.describe Tweet, type: :model do
  context '#validations' do
    it do
      should validate_length_of(:body).is_at_most(180).on(:create)
    end

    context 'when user had created in the last 24 hours with the same body' do
      let(:user) { create(:user) }
      let!(:tweet_1) { create(:tweet, user: user) }
      let(:tweet_2) { build(:tweet, user: user) }

      it 'fails the validation'  do
        expect { tweet_2.save! }.to raise_error(ActiveRecord::RecordInvalid, 'Validation failed: Created at Tweet with same body and less than 24 hours is not permitted')
      end
    end
  end
end
