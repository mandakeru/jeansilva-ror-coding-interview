# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates_length_of :body, maximum: 180
  validate :user_repeat_tweet

  def user_repeat_tweet
    tweet = Tweet.where(user_id: user_id).last

    return true if tweet.nil?

    if tweet.created_at > Date.current - 1.day && tweet.body == body
      errors.add :created_at, "Tweet with same body and less than 24 hours is not permitted"
    else
      true
    end
  end
end
