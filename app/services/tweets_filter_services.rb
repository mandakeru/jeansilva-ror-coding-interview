class TweetsFilterServices
  class << self
    def filter_tweets(params, page_limit)
      if params[:affiliated] == false
        user_ids = User.where(company_id: nil).pluck(:id)
        tweets = Tweet.where(user_id: user_ids).limit(page_limit)
      end

      tweets = Tweet.order("created_at ASC").limit(TWEETS_PER_PAGE).offset(offset)
    end
  end
end
